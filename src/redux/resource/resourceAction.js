import { UPDATE_RESOURCES_ACTION } from './resourceType';
export const updateResourceListAction = (resources = []) => {
    return {
        type: UPDATE_RESOURCES_ACTION,
        resources: resources
    };
};
