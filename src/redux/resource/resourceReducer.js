import { UPDATE_RESOURCES_ACTION } from './resourceType';
const initialState = {
    list: [],
    updated: new Date()
};
const resourceReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_RESOURCES_ACTION:
            return {
                ...state,
                list: action.resources,
                updated: new Date()
            };
        default:
            // console.log(new Error(`Action: ${action.type} Not found`));
            return state;
    }
};
export default resourceReducer;
