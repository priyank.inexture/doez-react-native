import { UPDATE_CLIENTS_ACTION } from './clientType';
export const updateClientListAction = (clients = []) => {
    return {
        type: UPDATE_CLIENTS_ACTION,
        clients: clients
    };
};
