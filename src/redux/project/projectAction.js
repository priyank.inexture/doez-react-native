import { UPDATE_PROJECTS_ACTION } from './projectType';
export const updateProjectListAction = (projects = []) => {
    return {
        type: UPDATE_PROJECTS_ACTION,
        projects: projects
    };
};
