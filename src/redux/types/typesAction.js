import { UPDATE_TYPES_ACTION } from './typesType';
export const updateTypesListAction = (availableTypes = {
    payment: [],
    allocation: [],
    priority: [],
    status: [],
    role: [],
    designation: [],
    technology: []
}) => {
    return {
        type: UPDATE_TYPES_ACTION,
        ...availableTypes
    };
};
