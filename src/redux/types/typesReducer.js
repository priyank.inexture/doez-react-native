import { UPDATE_TYPES_ACTION } from './typesType';
const initialState = {
    payment: [],
    allocation: [],
    priority: [],
    status: [],
    role: [],
    designation: [],
    technology: [],
    updated: new Date()
};
const typesReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_TYPES_ACTION:
            return {
                ...state,
                payment: action.payment,
                allocation: action.allocation,
                priority: action.priority,
                status: action.status,
                role: action.role,
                designation: action.designation,
                technology: action.technology,
                updated: new Date()
            };
        default:
            // console.log(new Error(`Action: ${action.type} Not found`));
            return state;
    }
};
export default typesReducer;
