import {
    SIGN_IN_ACTION,
    SIGN_OUT_ACTION,
    RESTORE_TOKEN_ACTION,
    LOADING_ACTION,
    CHANGE_HEADER_ACTION
} from './appType';
export const signInAction = (token = null) => {
    return {
        type: SIGN_IN_ACTION,
        isSignout: false,
        userToken: token
    };
};
export const signOutAction = () => {
    return {
        type: SIGN_OUT_ACTION,
        isSignout: true,
        userToken: null
    };
};
export const restoreTokenAction = (token = null) => {
    return {
        type: RESTORE_TOKEN_ACTION,
        userToken: token,
        isLoading: false
    };
};
export const loadingAction = (isLoading = true) => {
    return {
        type: LOADING_ACTION,
        isLoading: isLoading
    };
};
export const changeHeaderTitle = (title = 'Doez') => {
    return {
        type: CHANGE_HEADER_ACTION,
        headerTitle: title
    };
};
