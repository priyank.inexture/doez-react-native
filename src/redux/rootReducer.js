import { combineReducers } from 'redux';
import testReducer from './test/testReducer';
import appReducer from './app/appReducer';
import clientReducer from './client/clientReducer';
import projectReducer from './project/projectReducer';
import resourceReducer from './resource/resourceReducer';
import typesReducer from './types/typesReducer';

const rootReducer = combineReducers({
    test: testReducer,
    app: appReducer,
    client: clientReducer,
    project: projectReducer,
    resource: resourceReducer,
    types: typesReducer
});
export default rootReducer;
