import { TEST_ACTION } from './testType';
export const testAction = (randomValue = -1) => {
    return {
        type: TEST_ACTION,
        payload: randomValue
    };
};
