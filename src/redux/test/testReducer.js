import { TEST_ACTION } from './testType';
const initialState = {
    testList: []
};
const testReducer = (state = initialState, action) => {
    switch (action.type) {
        case TEST_ACTION:
            return {
                ...state,
                testList: [...state.testList, action.payload]
            };
        default:
            // console.log(new Error(`Action: ${action.type} Not found`));
            return state;
    }
};
export default testReducer;
