import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet,ScrollView } from 'react-native';
import { ListItem, Right, List, Icon,Title, Left, Body } from 'native-base';
const mapStateToProps = (state) => {
    return {
        resources: state.resource.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ResourceList({ navigation,resources }) {
    return (
        <ScrollView>
            <List>
                {resources.map(r =>
                    <ListItem key={r._id} onPress={() => navigation.navigate('Resource Detail', {
                        id: r._id
                    })}>
                        <Left>
                            <Body>
                                <Title style={styles.text}>
                                    {r.firstName} {r.lastName}
                                </Title>
                                {/* <Subtitle style={styles.subtext}>
                                    {p.teamLead.firstName} {p.teamLead.lastName}
                                </Subtitle> */}
                            </Body>
                        </Left>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>

                )}
            </List>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    text: {
        color: 'black'
    },
    subtext: {
        color: '#747474'
    }
});
ResourceList.propTypes = {
    resources: PropTypes.array,
    navigation: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ResourceList);
