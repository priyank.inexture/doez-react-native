import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
function ResourceDetailScreen({ route,navigation }) {
    const { id } = route.params;
    return (
        <Text>
            resource detail of {id}
        </Text>
    );
}
ResourceDetailScreen.propTypes = {
    navigation: PropTypes.any,
    route: PropTypes.any
};
export default ResourceDetailScreen;
