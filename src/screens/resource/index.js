import React from 'react';
// import { Text } from 'react-native';
import { View } from 'native-base';
import ResourceList from './ResourceList';
const ResourceScreen = (props) => (
    <View >
        <ResourceList {...props}/>
    </View>
);

export default ResourceScreen;
