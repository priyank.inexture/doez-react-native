import { Text, View } from 'native-base';
import React from 'react';
import PropTypes from 'prop-types';
function EditResourceScreen({ route,navigation }) {
    const { id } = route.params;
    return (
        <View>
            <Text>This is edit resource screen {id}</Text>
        </View>
    );
}
EditResourceScreen.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any
};
export default EditResourceScreen;
