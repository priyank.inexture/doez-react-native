import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet,ScrollView } from 'react-native';
import { ListItem, Right, List, Icon,Title, Left,Subtitle, Body } from 'native-base';
const mapStateToProps = (state) => {
    return {
        projects: state.project.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ProjectList({ navigation,projects }) {
    return (
        <ScrollView>
            <List>
                {projects.map(p =>
                    <ListItem key={p._id} onPress={() => navigation.navigate('Project Detail', {
                        id: p._id
                    })}>
                        <Left>
                            <Body>
                                <Title style={styles.text}>
                                    {p.name}
                                </Title>
                                <Subtitle style={styles.subtext}>
                                    {p.teamLead.firstName} {p.teamLead.lastName}
                                </Subtitle>
                            </Body>
                        </Left>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>

                )}
            </List>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    text: {
        color: 'black'
    },
    subtext: {
        color: '#747474'
    }
});
ProjectList.propTypes = {
    projects: PropTypes.array,
    navigation: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);
