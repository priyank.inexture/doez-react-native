import React,{ useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import { FlatList,View } from 'react-native';
import { connect } from 'react-redux';
import { Spinner } from 'native-base';
import { getProjectInfo } from '../../actions/projectAction';
import Field from '../../components/Field';
import ProjectClientResourceList from '../../components/ProjectClientResourceList';
// import { loadingAction} from '../../redux';
const mapStateToProps = (state,ownProps) => {
    const id = ownProps.route.params.id;
    console.log('id recived', id);
    return {
        project: state.project.list.find(p => p._id === id)
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        // loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading))
    };
};
function ProjectDetailScreen({ route,navigation }) {
    const { id } = route.params;
    const [isReady, setIsReady] = useState(false);
    const [project, setProject] = useState({ allocation: '',
        amount: 0,
        clientList: [],
        name: '',
        paymentType: '',
        priority: '',
        reviewer: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        status: '',
        teamLead: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        _id: '',
        tech: [],
        responsibilities: [] });
    useEffect(() => {
        // loadingActionProp(true);
        getProjectInfo(id)
            .then(({ data }) => {
                console.log(data);
                setProject({
                    ...data.project,
                    // eslint-disable-next-line max-nested-callbacks
                    tech: data.project.tech.map(t => t.value),
                    responsibilities: data.responsibilities
                });
            })
            .catch(err => console.log(err))
            .finally(() => setIsReady(true));
    }, []);
    return (
        <>
            {!isReady && <Spinner/>}
            {isReady &&
            <View>
                <FlatList

                    data={[
                        { title: 'Name', text: project.name },
                        { title: 'Payment Type', text: project.paymentType },
                        { title: 'Amount', text: project.amount },
                        { title: 'Allocation', text: project.allocation },
                        { title: 'Status', text: project.status },
                        { title: 'Priority', text: project.priority },
                        { title: 'Technologies', text: project.tech.join(', ') },
                        { title: 'Team Lead', text: project.teamLead ? `${project.teamLead.firstName} ${project.teamLead.lastName}` : null },
                        { title: 'Reviewer', text: project.reviewer ? `${project.reviewer.firstName} ${project.reviewer.lastName}` : null }
                    ]}
                    keyExtractor={(_,index) => index}
                    numColumns={1}
                    renderItem={({ item }) => (
                        <Field title={item.title} text={item.text}/>
                    )}
                />
                <View style={{ flex: 1 }}>
                    <ProjectClientResourceList responsibilities={project.responsibilities} clients={project.clientList} navigation={navigation}/>
                </View>
            </View>
            }
        </>
    );
}
ProjectDetailScreen.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailScreen);
