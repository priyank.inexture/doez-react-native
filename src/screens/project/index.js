import React from 'react';
// import { Text } from 'react-native';
import { View } from 'native-base';
import ProjectList from './projectList';
const ProjectScreen = (props) => (
    <View >
        <ProjectList {...props}/>
    </View>
);

export default ProjectScreen;
