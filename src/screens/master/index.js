import React from 'react';
import { SafeAreaView, Text } from 'react-native';

const MasterScreen = () => (
    <SafeAreaView>
        <Text>Screen: Master</Text>
    </SafeAreaView>
);

export default MasterScreen;
