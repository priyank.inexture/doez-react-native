import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
function ClientDetailScreen({ route,navigation }) {
    const { id } = route.params;
    return (
        <Text>
            client detail of {id}
        </Text>
    );
}
ClientDetailScreen.propTypes = {
    navigation: PropTypes.any,
    route: PropTypes.any
};
export default ClientDetailScreen;
