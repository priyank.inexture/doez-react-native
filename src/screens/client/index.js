import React from 'react';
// import { Text } from 'react-native';
import { View } from 'native-base';
import ClientList from './ClientList';
const ClientScreen = (props) => (
    <View >
        <ClientList {...props}/>
    </View>
);

export default ClientScreen;
