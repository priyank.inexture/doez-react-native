import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet,ScrollView } from 'react-native';
import { ListItem, Right, List, Icon,Title, Left, Body } from 'native-base';
const mapStateToProps = (state) => {
    return {
        clients: state.client.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ClientList({ navigation,clients }) {
    return (
        <ScrollView>
            <List>
                {clients.map(c =>
                    <ListItem key={c._id} onPress={() => navigation.navigate('Client Detail', {
                        id: c._id
                    })}>
                        <Left>
                            <Body>
                                <Title style={styles.text}>
                                    {c.firstName} {c.lastName}
                                </Title>
                                {/* <Subtitle style={styles.subtext}>
                                    {p.teamLead.firstName} {p.teamLead.lastName}
                                </Subtitle> */}
                            </Body>
                        </Left>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>

                )}
            </List>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    text: {
        color: 'black'
    },
    subtext: {
        color: '#747474'
    }
});
ClientList.propTypes = {
    clients: PropTypes.array,
    navigation: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ClientList);
