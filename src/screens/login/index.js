import React,{ useState } from 'react';
import { View,Text,StyleSheet } from 'react-native';
import { signInAction } from '../../redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CheckBox,Container, Header, Toast,Button,List, ListItem,InputGroup, Input, Icon } from 'native-base';
import { postLoginCredentials } from '../../actions/utilAction';
import { storeUserToken } from '../../utils/asyncStorage';
const mapStateToProps = (state) => {
    return {

    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        signInActionProp: (token) => dispatch(signInAction(token))
    };
};

function LoginScreen({ signInActionProp }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isSuperUser, setIsSuperUser] = useState(false);
    const handleButtonPress = () => {
        postLoginCredentials({
            user: {
                isSuperUser: isSuperUser,
                username: username,
                password: password
            }
        }).then(({ data: { token } }) => {
            signInActionProp(token);
            storeUserToken(token);
        }).catch((error) => {
            if (error.response.data.error) {
                Toast.show({
                    text: error.response.data.error,
                    type: 'danger'
                });
            } else {
                Toast.show({
                    text: 'Something went wrong!',
                    type: 'danger'
                });
            }
        });
    };
    return (
        <Container>
            <Header span>
                <View>
                    <Text style={styles.headerText}>Login</Text>
                    <Text style={styles.headerSubtext}>Sign in your account</Text>
                </View>
            </Header>
            <View>
                <List>
                    <ListItem>
                        <InputGroup>
                            <Icon name="ios-person" style={{ color: '#0A69FE' }} />
                            <Input
                                onChangeText={(text) => setUsername(text)}
                                value={username}

                                placeholder={'Username'} />
                        </InputGroup>
                    </ListItem>
                    <ListItem>
                        <InputGroup>
                            <Icon name="lock-closed" style={{ color: '#0A69FE' }} />
                            <Input
                                onChangeText={(text) => setPassword(text)}
                                value={password}
                                secureTextEntry={true}
                                placeholder={'Password'} />
                        </InputGroup>
                    </ListItem>
                    <ListItem>
                        <InputGroup style={styles.checkboxGroup}>
                            <CheckBox color='#0A69FE' checked={isSuperUser} onPress={() => setIsSuperUser(x => !x)}/>
                            <Text style={styles.superUserText}>Is super user!</Text>
                        </InputGroup>
                    </ListItem>
                </List>
                <Button
                    onPress={handleButtonPress}
                    block
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Login</Text>
                </Button>
            </View>
        </Container>
    );
}
LoginScreen.propTypes = {
    signInActionProp: PropTypes.func
};
const styles = StyleSheet.create({
    headerText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        marginTop: 24
    },
    headerSubtext: {
        textAlign: 'center',
        fontSize: 16,
        color: 'white',
        marginTop: 24
    },
    button: {
        marginHorizontal: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 18
    },
    superUserText: {
        marginLeft: 21,
        fontSize: 17,
        color: 'rgb(87, 87, 87)'
    },
    checkboxGroup: {
        marginTop: 5,
        marginBottom: 10
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
