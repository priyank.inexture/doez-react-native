/* eslint-disable no-nested-ternary */
import React,{ useEffect } from 'react';
import { Root ,Container } from 'native-base';
// import { Root ,Container, Header, Left, Body, Right, Title } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getUserToken, storeUserToken } from './utils/asyncStorage';
import { getApplicationData } from './actions/utilAction';
/* screens */
// import ListExample from './src/components/ListExample';
import SplashScreen from './screens/splash';
import LoginScreen from './screens/login';
import ProjectScreen from './screens/project';
import ProjectDetailScreen from './screens/project/ProjectDetailScreen';
import EditResourceScreen from './screens/project/EditResourceScreen';
import ClientScreen from './screens/client';
import ClientDetailScreen from './screens/client/ClientDetailScreen';
import ResourceScreen from './screens/resource';
import ResourceDetailScreen from './screens/resource/ResourceDetailScreen';
/* actions */
import { loadingAction,
    signInAction,
    updateClientListAction,
    updateProjectListAction,
    updateResourceListAction,
    updateTypesListAction
} from './redux';
// const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const mapStateToProps = (state) => {
    return {
        isLoading: state.app.isLoading,
        userToken: state.app.userToken,
        isSignout: state.app.isSignout,
        headerTitle: state.app.headerTitle
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading)),
        signInActionProp: (token) => dispatch(signInAction(token)),
        updateClientListActionProp: (clients) => dispatch(updateClientListAction(clients)),
        updateProjectListActionProp: (projects) => dispatch(updateProjectListAction(projects)),
        updateResourceListActionProp: (resources) => dispatch(updateResourceListAction(resources)),
        updateTypesListActionProp: (availableType) => dispatch(updateTypesListAction(availableType))
    };
};
const ProjectStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Project'>
            <Stack.Screen name="Project" component={ProjectScreen} />
            <Stack.Screen name="Project Detail" component={ProjectDetailScreen} />
            <Stack.Screen name="Edit Resource" component={EditResourceScreen} />
        </Stack.Navigator>
    );
};
const ResourceStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Resource'>
            <Stack.Screen name="Resource" component={ResourceScreen} />
            <Stack.Screen name="Resource Detail" component={ResourceDetailScreen} />
        </Stack.Navigator>
    );
};
const ClientStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Client'>
            <Stack.Screen name="Client" component={ClientScreen} />
            <Stack.Screen name="Client Detail" component={ClientDetailScreen} />
        </Stack.Navigator>
    );
};

function App({ updateClientListActionProp,updateProjectListActionProp,updateResourceListActionProp,updateTypesListActionProp,signInActionProp,loadingActionProp,isLoading,userToken,isSignout,headerTitle }) {
    useEffect(() => {
        getUserToken().then(token => {
            console.log('found token: ', token);
            if (token) {
                signInActionProp(token);
                storeUserToken(token);
                // storeUserToken(null);
            }
        }).finally(() => loadingActionProp(false));
    }, []);
    useEffect(() => {
        if (userToken) {
            getApplicationData().then(({ data }) => {
                // console.log('recived');
                // console.log(data);
                updateClientListActionProp(data.clients);
                updateProjectListActionProp(data.projects);
                updateResourceListActionProp(data.employees);
                updateTypesListActionProp(data.availableType);
            });
        }
    }, [userToken]);
    return (
        <Container>
            {userToken !== null && <></>
            // <Header>
            //     <Left/>
            //     <Body>
            //         <Title>{headerTitle}</Title>
            //     </Body>
            //     <Right />
            // </Header>
            }
            <Root>
                <NavigationContainer>
                    <Drawer.Navigator initialRouteName="Home">
                        {isLoading
                            ? <Drawer.Screen name='splash' component={SplashScreen}/>
                            : userToken === null
                                ? <Drawer.Screen
                                    name="SignIn"
                                    component={LoginScreen}
                                    options={{
                                        title: 'Sign in',
                                        // When logging out, a pop animation feels intuitive
                                        animationTypeForReplace: isSignout ? 'pop' : 'push'
                                    }}
                                />
                                : <>
                                    <Drawer.Screen name="Projects" component={ProjectStackNavigator} />
                                    <Drawer.Screen name="Resources" component={ResourceStackNavigator} />
                                    <Drawer.Screen name="Clients" component={ClientStackNavigator} />
                                </>
                        }
                    </Drawer.Navigator>
                </NavigationContainer>
            </Root>
        </Container>
    );
}
App.propTypes = {
    signInActionProp: PropTypes.func,
    loadingActionProp: PropTypes.func,
    updateClientListActionProp: PropTypes.func,
    updateProjectListActionProp: PropTypes.func,
    updateResourceListActionProp: PropTypes.func,
    updateTypesListActionProp: PropTypes.func,
    isLoading: PropTypes.bool,
    userToken: PropTypes.any,
    isSignout: PropTypes.bool,
    headerTitle: PropTypes.string
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
