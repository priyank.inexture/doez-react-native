import { APIKit,APIKitWithoutAuthorization } from '../utils/APIKit';
import { LOGIN_URL,TEST_URL } from '../constants/api';
export const getApplicationData = () => APIKit.get(TEST_URL);
export const postLoginCredentials = (data) => APIKitWithoutAuthorization.post(LOGIN_URL,data);
