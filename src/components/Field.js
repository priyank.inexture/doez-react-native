import React from 'react';
import PropTypes from 'prop-types';
import { View ,Text } from 'native-base';
import { StyleSheet } from 'react-native';
function Field({ title, text }) {
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{text ? text : 'Not Assigned'}</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row'
    },
    titleContainer: {
        flex: 1,
        margin: 5
    },
    textContainer: {
        flex: 2,
        margin: 5
    },
    title: {
        fontWeight: 'bold'
        // color: '#747474'
        // textAlign: 'right'
    },
    text: {

    }
});
Field.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string
};
export default Field;
