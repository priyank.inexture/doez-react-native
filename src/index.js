import React,{ useEffect,useState } from 'react';
import { Spinner } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
// import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import store from './redux/store';
import { registerRootComponent } from 'expo';
import App from './App';
function AppContainer(props) {
    const [isReady, setIsReady] = useState(false);
    useEffect(() => {
        async function loadFonts() {
            await Font.loadAsync({
                Roboto: require('native-base/Fonts/Roboto.ttf'),
                Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
                ...Ionicons.font
            });
            setIsReady(true);
        }
        loadFonts();
    }, []);
    return (
        <>
            {
                isReady
                    ? <Provider store={store}>
                        <App/>
                    </Provider>
                    : <Spinner/>
            }
        </>
    );
}
export default registerRootComponent(AppContainer);
